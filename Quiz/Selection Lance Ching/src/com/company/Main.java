package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[5];
        num[0] = -10;
        num[1] = 44;
        num[2] = 303;
        num[3] = 20;
        num[4] = -40;

        System.out.println("Before Selection Sort");
        printArrayElement(num);

        selectionsortAcd(num);

        System.out.println("After Selection Sort Accending");
        printArrayElement(num);

        ssortDcdSmlEnd(num);
        System.out.println("After Selection Sort Decending Small at the end");
        printArrayElement(num);

        selectionsortAcd(num);
        System.out.println("After Selection Sort Accending");
        printArrayElement(num);

        sSortDcdlargeFirst(num);
        System.out.println("After Selection Sort Decending Largest at the beginning");
        printArrayElement(num);

    }

    private static void selectionsortAcd(int[] arr) {
        int minValue, minIndex,temp=0;
        for (int i=0; i< arr.length;i++) {
            minValue= arr[i];
            minIndex = i;
            for (int j=i+1; j< arr.length; j++) {
                if (arr[j] < minValue) {
                    minValue= arr[j];
                    minIndex= j;
                }
            }
            if (minValue < arr [i])
            {
                temp= arr[i];
                arr[i]= arr[minIndex];
                arr[minIndex]= temp;
            }

        }
    }

    private static void printArrayElement(int[] arr) {
        for (int j : arr) {
            System.out.println(j + " ");
        }
    }

    private static void sSortDcdlargeFirst(int[] arr) {
        int maxValue, maxIndex, temp = 0;
        for (int i = 0; i < arr.length; i++) {
            maxValue = arr[i];
            maxIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] > maxValue) {
                    maxValue = arr[j];
                    maxIndex = j;
                }
            }
            if (maxValue > arr[i]) {
                temp = arr[i];
                arr[i] = arr[maxIndex];
                arr[maxIndex] = temp;
            }

        }
    }


    private static void ssortDcdSmlEnd(int[] arr)
    {
       int minValue, minIndex, temp = 0;
       for (int i = arr.length-1; i >0; i--) {
           minValue = arr[i];
            minIndex = i;
            for (int j = i - 1; j >=0; j--) {
                if (arr[j] < minValue) {
                    minValue = arr[j];
                    minIndex = j;
                }
            }
            if (minValue < arr[i]) {
                temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }

        }
    }
}


