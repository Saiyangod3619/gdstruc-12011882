package com.gdstruc;

import java.util.Objects;

public class CardStack {


    private String name;

    @Override
    public String toString() {
        return "CardStack{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardStack cardStack = (CardStack) o;
        return Objects.equals(name, cardStack.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CardStack(String name) {
        this.name = name;
    }
}
