package com.gdstruc;

import java.util.LinkedList;
import java.util.ListIterator;

public class LinkStack {
    private LinkedList<CardStack> stack;

    public LinkStack()
    {
        stack = new LinkedList<CardStack>();

    }

    public void push(CardStack cardStack)
    {
        stack.push(cardStack);
    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public CardStack pop()
    {
        return stack.pop();
    }

    public CardStack peek()
    {
        return stack.peek();
    }

    public void printStack()
    {
        ListIterator<CardStack> iterator = stack.listIterator();
        System.out.println("printing Card Stack");
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    public int numberOfElement(){
        final int length = stack.toArray().length;
        return length;
    }
}
