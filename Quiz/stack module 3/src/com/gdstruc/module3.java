package com.gdstruc;


import java.util.Scanner;

public class module3 {
public static void main(String[]args){


    int num;
    LinkStack stack = new LinkStack();
    LinkCard myStack = new LinkCard();
    stack.push(new CardStack("ace 1"));
    stack.push(new CardStack("spade 1"));
    stack.push(new CardStack("diamond1"));
    stack.push(new CardStack("heart1"));
    stack.push(new CardStack("ace2"));
    stack.push(new CardStack("ace3"));
    stack.push(new CardStack("ace4"));
    stack.push(new CardStack("ace5"));
    stack.push(new CardStack("ace6"));
    stack.push(new CardStack("ace7"));
    stack.push(new CardStack("ace8"));
    stack.push(new CardStack("ace9"));
    stack.push(new CardStack("ace10"));
    stack.push(new CardStack("spade2"));
    stack.push(new CardStack("spade3"));
    stack.push(new CardStack("spade4"));
    stack.push(new CardStack("spade5"));
    stack.push(new CardStack("spade6"));
    stack.push(new CardStack("spade7"));
    stack.push(new CardStack("spade8"));
    stack.push(new CardStack("spade9"));
    stack.push(new CardStack("diamond2"));
    stack.push(new CardStack("diamond3"));
    stack.push(new CardStack("diamond4"));
    stack.push(new CardStack("diamond5"));
    stack.push(new CardStack("diamond6"));
    stack.push(new CardStack("diamond7"));
    stack.push(new CardStack("diamond9"));
    stack.push(new CardStack("heart2"));
    stack.push(new CardStack("heart3"));

    num =0;

    if (stack.isEmpty()) {
        System.out.println(" out of stack! program end");
    }
    while (!stack.isEmpty()){

            while ((num < 1 ) || (num > 5) ) {
                System.out.println("Enter number of cards (1-5)");
                Scanner scanner = new Scanner(System.in);
                num = scanner.nextInt();
            }

            System.out.println("Popping " + num + " times");
            for (int i=1; i <= num; i++)
            {
                myStack.push(new Card(stack.peek().getName()));
                System.out.println("Popping"+ stack.pop());

                if (stack.isEmpty()) {
                    i = num;
                    System.out.println("out of stack !!");
                }
            }
        System.out.println("List of cards that the player is currently holding");
        stack.printStack();
        System.out.println("Number of remaining cards in the player deck "+ stack.numberOfElement());
        System.out.println("Number of cards in the discarded pile "+myStack.numberOfElement());
        num =0;
    }

    }

}
