package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[5];
        num[0] = 459;
        num[1] = 845;
        num[2] = 477;
        num[3] = 497;
        num[4] = 448;

        System.out.println("Before Bubble Sort");
        printArrayElement(num);

        bubbleSort(num);

        System.out.println("After Bubble Sort");
        printArrayElement(num);
    }
//
    private static void bubbleSort(int[] arr)
    {
        for (int lastSortIndex = arr.length - 1; lastSortIndex > 0; lastSortIndex--)
        {
            for (int i = 0; i < lastSortIndex; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i]= arr[i+1];
                    arr[i + 1] = temp;
                }
            }
        }
    }
    private static void printArrayElement(int[] arr)
    {
        for (int j : arr)
        {
            System.out.println(j +" ");
        }
    }
}
