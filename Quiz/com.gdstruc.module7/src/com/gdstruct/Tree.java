package com.gdstruct;

public class Tree {

    private  Node root;


    public void insert(int value)
    {
        if (root == null)
        {
            root = new Node (value);
        }
        else
        {
            root.insert(value);
        }
    }

    public void traversalInOrder()
    {
        if (root != null)
        {
            root.travarsalInOrder();
        }
    }

    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    public Node getMin()
    {
        if (root != null) {

            return root.getMin(root.getData());
        }
        return null;
    }


    public Node getMax()
    {
        if (root != null) {

            return root.getMax(root.getData());
        }
        return null;
    }

    public void TransInOrderDecending()
    {
        if (root != null)
        {
            root.TransInOrderDecending();
        }
    }




}
