package com.gdstruct;



public class Node{

    private int data;
    private Node rightChild;
    private Node leftChild;




    public Node(int data) {
        this.data = data;
    }

    public void insert(int value)
    {
        if (value == data)
        {
            return ;
        }
        if ( value < data)
        {
            if (leftChild == null)
            {
                leftChild = new Node( value);
            }
            else
            {
                leftChild.insert(value);
            }
        }
        else
        {
            if ( rightChild == null)
            {
                rightChild = new Node (value);
            }
            else
            {
                rightChild.insert(value);
            }
        }
    }
    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public void travarsalInOrder()
    {
        if (leftChild != null)
        {
            leftChild.travarsalInOrder();
        }
        System.out.println( " Data accending = "+ data);
        if ( rightChild != null)
        {
            rightChild.travarsalInOrder();
        }
    }

    @Override
    public String toString() {
        return "node{" +
                "data=" + data +
                '}';
    }

    public Node get(int value) {
        if (value == data) {
            return this;
        }
        if (value < data) {
            if (leftChild != null) {
                return leftChild.get(value);
            }

        } else {
            if (rightChild != null) {
                return rightChild.get(value);
            }
        }
        return null;
    }

    public Node getMin(int tempMin) {


        if (leftChild == null) {

            return get(tempMin);
        }
        else {
                if (tempMin > leftChild.getData()) {

                tempMin = leftChild.getData();

                }
            return leftChild.getMin(tempMin);
        }
    }


    public Node getMax(int tempMax) {


        if (rightChild == null) {

            return get(tempMax);
        }
        else {
                if (tempMax < rightChild.getData()) {

                tempMax = rightChild.getData();

                }
            return rightChild.getMax(tempMax);
        }

    }


    public void TransInOrderDecending()
    {
        if ( rightChild != null)
        {
            rightChild.TransInOrderDecending();
        }

        System.out.println( " Data decending  = "+ data);


        if (leftChild != null)
        {
            leftChild.TransInOrderDecending();
        }

    }
}
