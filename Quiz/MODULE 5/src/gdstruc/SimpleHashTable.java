package gdstruc;

public class SimpleHashTable {
    private StoredPlayer[] hashTable;

    public SimpleHashTable()
    {
        hashTable = new StoredPlayer[10];
    }

    private int hashKey(String key)
    {
        return key.length()% hashTable.length;
    }

    public void put(String key, Player value)
    {
        int hashKey = hashKey(key);

        if (isOccupied( hashKey))
        {
            int stoppingIndex =hashKey;

            if ( hashKey == hashTable.length -1)
            {
                hashKey =0;
            }
            else
            {
                hashKey++;
            }
            while ( isOccupied(hashKey) && hashKey != stoppingIndex)
            {
                hashKey = (hashKey+1)% hashTable.length;
            }
        }
        if (isOccupied(hashKey))
        {
            System.out.println(" element already occupied"+ hashKey);
        }
        else
        {
            hashTable[hashKey] = new StoredPlayer(key,value);
        }
    }

    private  boolean  isOccupied(int index)
    {
        return hashTable[index] != null;
    }

    public Player get(String key)
    {
        int hashKey = findkey(key);

        if (hashKey == -1)
        {
            return  null;
        }
        return hashTable[hashKey].value;
    }

    private int findkey(String key)
    {
        int hashKey = hashKey(key);

        if (hashTable[hashKey] != null && hashTable[hashKey].key.equals(key)) {
            return hashKey;
        }

        int stoppingIndex = hashKey;

        if (hashKey == hashTable.length - 1) {
            hashKey = 0;
        } else {
            hashKey++;
        }
        while (hashKey != stoppingIndex && hashTable[hashKey] != null &&
           ! hashTable[hashKey].key.equals(key))
        {
            hashKey = (hashKey + 1) % hashTable.length;
        }
        if (hashTable[hashKey] != null && hashTable[hashKey].key.equals(key))
        {
            return hashKey;
        }
        return -1;

    }

    public  void printTable()
    {
        for ( int i=0 ; i < hashTable.length; i++ )
        {
            if (hashTable[i] != null)
            {
                System.out.println("Element " + i + " " + hashTable[i].value);
            }
            else
            {
                System.out.println("Element "+ i + "null");
            }

        }
    }

    public void remove(String key)
    {
        int hashKey = findkey(key);

        if (hashKey == -1)
        {
           System.out.println(" non existance");
        }
         hashTable[hashKey].value = null;

    }
}
