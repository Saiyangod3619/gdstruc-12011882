package gdstruc;

public class module5 {
    public static void main(String[] args) {


        Player ploo = new Player(1, "Plooful");
        Player wardell = new Player(2, "TSM wardell");
        Player DeadlyJimmy = new Player(3, "DeadlyJimmy");
        Player Subroza = new Player(4, "Subroza");
        Player annieDro = new Player(5, "C9 annie");

        SimpleHashTable hashtable = new SimpleHashTable();
        hashtable.put(ploo.getUserName(), ploo);
        hashtable.put(wardell.getUserName(), wardell);
        hashtable.put(DeadlyJimmy.getUserName(), DeadlyJimmy);
        hashtable.put(Subroza.getUserName(), Subroza);
        hashtable.put(annieDro.getUserName(), annieDro);

        hashtable.printTable();

        System.out.println(hashtable.get("Subroza"));
        hashtable.remove("Subroza");
        hashtable.printTable();
    }
}





