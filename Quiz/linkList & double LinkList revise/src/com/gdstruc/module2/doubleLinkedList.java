package com.gdstruc.module2;

public class doubleLinkedList {
    private PlayerNode head;

    public void addToEnd(Player player){
        PlayerNode playerNode = new PlayerNode(player);
        if (head == null){
            head = playerNode;
        }
        else{
            PlayerNode current = head;
            while (current.getNextPlayer()!= null){
                current=current.getNextPlayer();
            }
            current.setNextPlayer(playerNode);
            playerNode.setPrevPlayer(current);
        }
    }

    public void addToFront(Player player){
        PlayerNode playerNode = new PlayerNode(player);
        if (head == null){
            head = playerNode;
        }
        else{
            playerNode.setNextPlayer(head);
            head.setPrevPlayer(playerNode);
            head = playerNode;
        }
    }

    public void printList(){
        PlayerNode current = head;
        System.out.println("HEAD-> ");
        while (current != null){
            System.out.println(current.getPlayer());
            System.out.println("NEXT -> ");
            current = current.getNextPlayer();

        }
        System.out.println("NULL");
        current = head;
        while (current.getNextPlayer()!= null){
            current=current.getNextPlayer();
        }


        while ( current != null){
            System.out.println(current.getPlayer());
            System.out.println("NEXT -> ");
            current = current.getPrevPlayer();
        }
        System.out.println("NULL");
    }

}
