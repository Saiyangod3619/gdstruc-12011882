package com.gdstruc.module2;

public class PlayerLinkList {
    private PlayerNode head;


    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

   public void removeFront(){
        PlayerNode temp = head;
        head = head.getNextPlayer();
    }

    public void printList(){
        PlayerNode current =head;
        System.out.print("HEAD -> ");
        while (current != null){
            System.out.println(current.getPlayer());
            System.out.print(" next-> ");
            current =current.getNextPlayer();

        }
        System.out.println("null");

    }


    public int printLength(){
        int count =0;
        PlayerNode temp = head;
        while (temp != null){
            temp = temp.getNextPlayer();
            count++;
        }
        return count;
    }

    public boolean containItem(Player player){
        PlayerNode current  = head;
        while (current != null){
            if (current.getPlayer() == player){
                return true;
            }
            current = current.getNextPlayer();
        }
        return false;
    }

    public int indexItem(Player player){
        PlayerNode current = head;
        int index = 0;
        while (current != null){
            if (current.getPlayer() == player){
                return index;
            }
            current = current.getNextPlayer();
            index++;
        }
        return -1;
    }
}
