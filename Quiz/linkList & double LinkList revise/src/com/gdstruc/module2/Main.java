package com.gdstruc.module2;

public class Main {
    public static void main(String[] args) {

        Player asuna = new Player(1, "Asuna",100);
        Player lethalBacon = new Player(2,"lethalBacon",205);
        Player hpDeskjet = new Player(3, "HPDeskjet", 34);

        PlayerLinkList playerLinkList = new PlayerLinkList();

        playerLinkList.addToFront(asuna);
        playerLinkList.addToFront(lethalBacon);
        playerLinkList.addToFront(hpDeskjet);


        playerLinkList.printList();
        System.out.println(playerLinkList.printLength());
        playerLinkList.removeFront();
        playerLinkList.printList();
        System.out.println(playerLinkList.printLength());

        System.out.println(playerLinkList.containItem(asuna));
        System.out.println(playerLinkList.containItem(hpDeskjet));

        System.out.println(playerLinkList.indexItem(asuna));
        System.out.println(playerLinkList.indexItem(hpDeskjet));


        System.out.println(" DOUBLE LINK LIST");

        doubleLinkedList playerLinkedList = new doubleLinkedList();
        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(hpDeskjet);
        playerLinkedList.addToEnd(lethalBacon);

        playerLinkedList.printList();
    }
}
