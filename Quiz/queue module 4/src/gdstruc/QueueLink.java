package gdstruc;

import java.util.LinkedList;
import java.util.ListIterator;

public class QueueLink {
    private LinkedList<Player> queue;

    public QueueLink()
    {
        queue = new LinkedList<Player>();
    }

    public void enqueue(Player player)
    {
        queue.addLast(player);
    }

    public boolean isEmpty()
    {
        return queue.isEmpty();
    }

    public Player dequeue()
    {
        return queue.removeFirst();
    }
    public Player peek()
    {
         return queue.peekFirst();
    }

    public void printQueue()
    {
        ListIterator<Player> iterator = queue.listIterator();
        System.out.println("Printing Player Queue");
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());

        }
    }

    public  int numberOfElement()
    {
        final  int length = queue.toArray().length;
        return length;
    }

    public void printLimitList( int  elementNumber)
    {
        int x = 1;
        ListIterator<Player> iterator = queue.listIterator();
        System.out.println("Printing Player Queue");
        while (iterator.hasNext() && x <= elementNumber)
        {
            System.out.println(iterator.next());
            x++;
        }

    }

}
